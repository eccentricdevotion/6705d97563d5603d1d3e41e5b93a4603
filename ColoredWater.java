package me.bryan.color;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Cauldron;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class ColoredWater extends JavaPlugin implements Listener {

    private final HashMap<Byte, Color> dyeColor = new HashMap();

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        dyeColor.put((byte) 0, Color.BLACK);
        dyeColor.put((byte) 1, Color.WHITE);
        dyeColor.put((byte) 2, Color.ORANGE);
        dyeColor.put((byte) 3, Color.fromRGB(199, 78, 189));
        dyeColor.put((byte) 4, Color.fromRGB(58, 179, 218));
        dyeColor.put((byte) 5, Color.YELLOW);
        dyeColor.put((byte) 6, Color.LIME);
        dyeColor.put((byte) 7, Color.fromRGB(243, 139, 170));
        dyeColor.put((byte) 8, Color.GRAY);
        dyeColor.put((byte) 9, Color.SILVER);
        dyeColor.put((byte) 10, Color.fromRGB(22, 156, 156));
        dyeColor.put((byte) 11, Color.PURPLE);
        dyeColor.put((byte) 12, Color.BLUE);
        dyeColor.put((byte) 13, Color.fromRGB(131, 84, 50));
        dyeColor.put((byte) 14, Color.GREEN);
        dyeColor.put((byte) 15, Color.RED);
    }

    @EventHandler
    public void onBlockPlayer(BlockPlaceEvent event) {
        Block block = event.getBlock();
        if (block.getType() != Material.CAULDRON) {
            return;
        }
        Location loc = block.getLocation();
        ArmorStand armorStand = loc.getWorld().spawn(loc.add(0.5D, 0.25D, 0.5D), ArmorStand.class);
        armorStand.setVisible(false);
        armorStand.setGravity(false);
        armorStand.setInvulnerable(true);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        if (e.getClickedBlock().getType() != Material.CAULDRON) {
            return;
        }
        if (player.getInventory().getItemInMainHand().getType() == Material.WATER_BUCKET) {
            Location loc = e.getClickedBlock().getLocation();
            for (Entity entity : loc.getWorld().getNearbyEntities(loc.add(0.5D, 0.0D, 0.5D), 0.1D, 0.4D, 0.1D)) {
                if (!(entity instanceof ArmorStand)) {
                    continue;
                }
                ArmorStand armorStand = (ArmorStand) entity;
                if (armorStand.getEquipment().getHelmet().getType() == Material.LEATHER_BOOTS) {
                    e.setCancelled(true);
                    return;
                }
            }
        }
        if (player.getInventory().getItemInMainHand().getType() == Material.BUCKET) {
            Location loc = e.getClickedBlock().getLocation();
            for (Entity entity : loc.getWorld().getNearbyEntities(loc.add(0.5D, 0.0D, 0.5D), 0.1D, 0.4D, 0.1D)) {
                if (!(entity instanceof ArmorStand)) {
                    continue;
                }
                ArmorStand armorStand = (ArmorStand) entity;
                if (armorStand.getEquipment().getHelmet().getType() == Material.LEATHER_BOOTS) {
                    armorStand.setHelmet(null);
                    ItemStack inMainHand = player.getInventory().getItemInMainHand();
                    inMainHand.setAmount(inMainHand.getAmount() - 1);
                    player.getInventory().addItem(new ItemStack[]{new ItemStack(Material.WATER_BUCKET)});
                    player.playSound(player.getLocation(), Sound.ITEM_BUCKET_FILL, 1.0F, 1.0F);
                    e.setCancelled(true);
                    return;
                }
            }
        } else {
            if (player.getInventory().getItemInMainHand().getType() != Material.INK_SACK) {
                return;
            }
            BlockState state = e.getClickedBlock().getState();
            Cauldron cauldron = (Cauldron) state.getData();
            if (cauldron.isEmpty()) {
                return;
            }
            cauldron.setData((byte) 0);
            state.update();

            Location location = e.getClickedBlock().getLocation();
            for (Entity entity : location.getWorld().getNearbyEntities(location.add(0.5D, 0.0D, 0.5D), 0.1D, 0.4D, 0.1D)) {
                if (!(entity instanceof ArmorStand)) {
                    continue;
                }
                ArmorStand as = (ArmorStand) entity;
                ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                LeatherArmorMeta meta = (LeatherArmorMeta) boots.getItemMeta();
                meta.setColor(dyeColor.get(player.getInventory().getItemInMainHand().getData().getData()));
                meta.setUnbreakable(true);
                boots.setItemMeta(meta);
                boots.setDurability((short) 1);
                as.setHelmet(boots);
                ItemStack inMainHand = player.getInventory().getItemInMainHand();
                inMainHand.setAmount(inMainHand.getAmount() - 1);
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_SPLASH, 1.0F, 1.0F);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Block block = e.getBlock();
        if (block.getType() != Material.CAULDRON) {
            return;
        }
        Location location = block.getLocation();
        for (Entity entity : location.getWorld().getNearbyEntities(location.add(0.5D, 0.0D, 0.5D), 0.1D, 0.4D, 0.1D)) {
            if (!(entity instanceof ArmorStand)) {
                continue;
            }
            entity.remove();
        }
    }
}
